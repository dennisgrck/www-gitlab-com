---
layout: job_family_page
title: "Treasury"
---

### Levels 

## Senior Treasury Manager 

The Senior Treasury Manager reports to the [Corporate Controller](https://about.gitlab.com/job-families/finance/corporate-controller/#director-corporate-controller)

#### Senior Treasury Manager Job Grade

The Senior Treasury Manager is a [grade #9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Treasury Manager Responsibilities

- Monitor financial risk exposures including foreign exchange, recommend and execute the Company‘s financial risk management strategies and potentially hedging programs
- Open bank accounts in new foreign entities
- Ensure debt covenant compliance
- Develop and manage relationships with banking institutions and investment groups and manage our adherence to our investment policy
- Support subsidiary operations in treasury matters including cash management and banking relations
- Supports treasury and cash management operations and helps develop strategies to maximize efficiencies, safeguard assets and minimize costs 
- Conducts investing activities
- Support internal customers as needed
- Support treasury process improvement and systems integration efforts including publishing and maintaining Treasury Policies and Procedures, maintaining treasury applications, and implementing treasury best practices
- Perform treasury reporting and budgeting/forecasting responsibilities including cash flow and financial income/expense
- Support internal and external audit, public filing, and investor relations activities as needed
- Responsible for performing internal controls for treasury processes, identifying areas which need improvement and implementing plans to correct and prevent problems
- Additional responsibilities/projects as required

#### Senior Treasury Manager Requirements

- B.A. in Finance required; MBA or MA in Finance preferred. CFA helpful but not required.
- Minimum 8 years finance experience involving analyzing currency risks, executing hedging strategies utilizing FX, and experience in investment strategy.
- Experience with derivatives accounting rules and hedge accounting requirements preferred.
- Must demonstrate overall business acumen and understanding of international treasury operations.  
- Must have excellent interpersonal and communication skills for success in this position (verbal, written, presentations, and consultative).
- Experience with ERP systems such as NetSuite.
- Ability to use GitLab.

### Performance Indicators

- No deviations from Investment Policy and Compliance with all Loan covenants
- Average Return of Return on Investments vs. Benchmarks
- Develop Hedging Strategy that will minimize Foreign Exchange Exposure
- Work with Sales and Billing to Develop Credit Policy that minimizes risk of Bad Debt not to exceed .5% per year for write offs on average accounts receivable.
- Develop cash forecasting and measure accuracy of actual vs. forecast
- Measure payments released timely and develop metrics
- ACH payments vs. manual or wires (Achieve over 90% ACH payments)

## Director - Treasury

The Senior Treasury Manager reports to the Corporate Controller

#### Director - Treasury Job Grade

The Director - Treasury is a [grade #10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Treasury Manager Responsibilities

- Is responsible for financial risk exposures including foreign exchange, recommend, drive and execute the Company‘s financial risk management strategies and potentially hedging programs
- Responsible for bank strategy and work to open bank accounts in new foreign entities
- Responsible for debt covenant compliance
- Will drive and be responsible to develop and manage relationships with banking institutions and investment groups and direct our adherence to our investment policy
- Responsible for subsidiary operations in treasury matters including cash management and banking relations
- Responsible for  treasury and cash management operations and helps develop strategies to maximize efficiencies, safeguard assets and minimize costs
- Drives investing activities
- Support internal customers
- Responsible for treasury process improvement and systems integration efforts including publishing and maintaining Treasury Policies and Procedures, maintaining treasury applications, and implementing treasury best practices
- Responsible for and drive treasury reporting and budgeting/forecasting responsibilities including cash flow and financial income/expense
- Main contact related to treasury operations for the internal and external audit, public filing, and investor relations activities
- Responsible for performing internal controls for treasury processes, identifying areas which need improvement and implementing plans to correct and prevent problems
- Additional responsibilities/projects as required

#### Director - Treasury Requirements

- B.A. in Finance required; MBA or MA in Finance preferred. CFA helpful but not required.
- Minimum 8-12 years finance experience involving analyzing currency risks, executing hedging strategies utilizing FX, and experience in investment strategy.
- Experience with derivatives accounting rules and hedge accounting requirements preferred.
- Must demonstrate overall business acumen and understanding of international treasury operations and have been responsible for the activities for at least 4 years
- Must have excellent interpersonal and communication skills for success in this position (verbal, written, presentations, and consultative).
- Experience with ERP systems such as NetSuite.
- Ability to use GitLab.
- Performance Indicators
- No deviations from Investment Policy and Compliance with all Loan covenants
- Average Return of Return on Investments vs. Benchmarks
- Develop Hedging Strategy that will minimize Foreign Exchange Exposure
- Work with Sales and Billing to Develop Credit Policy that minimizes risk of Bad Debt not to exceed .5% per year for write offs on average accounts receivable.
- Develop cash forecasting and measure accuracy of actual vs. forecast
- Measure payments released timely and develop metrics
- ACH payments vs. manual or wires (Achieve over 90% ACH payments)

## Career Ladder

The next step in the Treasury job family is to move to the Principal Accounting Officer job family.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.

* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Meet with PAO for 50 minutes
- Meet with Key Finance Leaders - for 30 minutes each
- Meet with VP of Finance for 50 minutes
- Meet with CFO for 50 minutes

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
