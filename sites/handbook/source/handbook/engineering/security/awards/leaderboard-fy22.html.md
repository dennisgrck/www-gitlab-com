---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY22

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@djadmin](https://gitlab.com/djadmin) | 1 | 3340 |
| [@vitallium](https://gitlab.com/vitallium) | 2 | 1500 |
| [@ali-gitlab](https://gitlab.com/ali-gitlab) | 3 | 1440 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 4 | 1370 |
| [@dblessing](https://gitlab.com/dblessing) | 5 | 1320 |
| [@tkuah](https://gitlab.com/tkuah) | 6 | 1240 |
| [@manojmj](https://gitlab.com/manojmj) | 7 | 1060 |
| [@sabrams](https://gitlab.com/sabrams) | 8 | 1030 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 9 | 900 |
| [@engwan](https://gitlab.com/engwan) | 10 | 840 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 11 | 830 |
| [@theoretick](https://gitlab.com/theoretick) | 12 | 700 |
| [@ifrenkel](https://gitlab.com/ifrenkel) | 13 | 700 |
| [@dsearles](https://gitlab.com/dsearles) | 14 | 700 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 15 | 700 |
| [@mrincon](https://gitlab.com/mrincon) | 16 | 640 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 17 | 620 |
| [@xanf](https://gitlab.com/xanf) | 18 | 620 |
| [@.luke](https://gitlab.com/.luke) | 19 | 620 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 20 | 610 |
| [@mksionek](https://gitlab.com/mksionek) | 21 | 600 |
| [@philipcunningham](https://gitlab.com/philipcunningham) | 22 | 600 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 23 | 600 |
| [@leipert](https://gitlab.com/leipert) | 24 | 580 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 25 | 580 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 26 | 500 |
| [@alexpooley](https://gitlab.com/alexpooley) | 27 | 500 |
| [@pks-t](https://gitlab.com/pks-t) | 28 | 500 |
| [@shreyasagarwal](https://gitlab.com/shreyasagarwal) | 29 | 500 |
| [@ash2k](https://gitlab.com/ash2k) | 30 | 500 |
| [@dsatcher](https://gitlab.com/dsatcher) | 31 | 500 |
| [@stanhu](https://gitlab.com/stanhu) | 32 | 480 |
| [@balasankarc](https://gitlab.com/balasankarc) | 33 | 470 |
| [@10io](https://gitlab.com/10io) | 34 | 470 |
| [@whaber](https://gitlab.com/whaber) | 35 | 400 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 36 | 400 |
| [@wortschi](https://gitlab.com/wortschi) | 37 | 400 |
| [@brodock](https://gitlab.com/brodock) | 38 | 400 |
| [@ggeorgiev_gitlab](https://gitlab.com/ggeorgiev_gitlab) | 39 | 400 |
| [@markrian](https://gitlab.com/markrian) | 40 | 360 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 41 | 340 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 42 | 320 |
| [@serenafang](https://gitlab.com/serenafang) | 43 | 320 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 44 | 300 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 45 | 300 |
| [@mikeeddington](https://gitlab.com/mikeeddington) | 46 | 300 |
| [@matteeyah](https://gitlab.com/matteeyah) | 47 | 300 |
| [@jameslopez](https://gitlab.com/jameslopez) | 48 | 300 |
| [@viktomas](https://gitlab.com/viktomas) | 49 | 300 |
| [@gonzoyumo](https://gitlab.com/gonzoyumo) | 50 | 300 |
| [@mwoolf](https://gitlab.com/mwoolf) | 51 | 250 |
| [@kerrizor](https://gitlab.com/kerrizor) | 52 | 220 |
| [@pshutsin](https://gitlab.com/pshutsin) | 53 | 220 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 54 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 55 | 200 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 56 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 57 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 58 | 200 |
| [@jcaigitlab](https://gitlab.com/jcaigitlab) | 59 | 200 |
| [@peterhegman](https://gitlab.com/peterhegman) | 60 | 200 |
| [@seanarnold](https://gitlab.com/seanarnold) | 61 | 180 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 62 | 170 |
| [@toupeira](https://gitlab.com/toupeira) | 63 | 170 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 64 | 150 |
| [@splattael](https://gitlab.com/splattael) | 65 | 140 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 66 | 140 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 67 | 140 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 68 | 140 |
| [@mkozono](https://gitlab.com/mkozono) | 69 | 140 |
| [@twk3](https://gitlab.com/twk3) | 70 | 130 |
| [@ekigbo](https://gitlab.com/ekigbo) | 71 | 130 |
| [@nfriend](https://gitlab.com/nfriend) | 72 | 120 |
| [@garyh](https://gitlab.com/garyh) | 73 | 120 |
| [@cngo](https://gitlab.com/cngo) | 74 | 110 |
| [@minac](https://gitlab.com/minac) | 75 | 110 |
| [@tomquirk](https://gitlab.com/tomquirk) | 76 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 77 | 100 |
| [@cablett](https://gitlab.com/cablett) | 78 | 100 |
| [@ahegyi](https://gitlab.com/ahegyi) | 79 | 90 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 80 | 90 |
| [@vsizov](https://gitlab.com/vsizov) | 81 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 82 | 80 |
| [@ck3g](https://gitlab.com/ck3g) | 83 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 84 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 85 | 80 |
| [@tancnle](https://gitlab.com/tancnle) | 86 | 80 |
| [@acroitor](https://gitlab.com/acroitor) | 87 | 80 |
| [@rcobb](https://gitlab.com/rcobb) | 88 | 80 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 89 | 80 |
| [@brytannia](https://gitlab.com/brytannia) | 90 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 91 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 92 | 60 |
| [@lulalala](https://gitlab.com/lulalala) | 93 | 60 |
| [@dzaporozhets](https://gitlab.com/dzaporozhets) | 94 | 60 |
| [@jannik_lehmann](https://gitlab.com/jannik_lehmann) | 95 | 60 |
| [@jivanvl](https://gitlab.com/jivanvl) | 96 | 60 |
| [@ebaque](https://gitlab.com/ebaque) | 97 | 60 |
| [@farias-gl](https://gitlab.com/farias-gl) | 98 | 60 |
| [@sming-gitlab](https://gitlab.com/sming-gitlab) | 99 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 100 | 50 |
| [@pslaughter](https://gitlab.com/pslaughter) | 101 | 40 |
| [@dgruzd](https://gitlab.com/dgruzd) | 102 | 40 |
| [@f_caplette](https://gitlab.com/f_caplette) | 103 | 40 |
| [@afontaine](https://gitlab.com/afontaine) | 104 | 40 |
| [@proglottis](https://gitlab.com/proglottis) | 105 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 106 | 30 |
| [@ifarkas](https://gitlab.com/ifarkas) | 107 | 30 |
| [@nmezzopera](https://gitlab.com/nmezzopera) | 108 | 30 |
| [@nmilojevic1](https://gitlab.com/nmilojevic1) | 109 | 30 |
| [@ohoral](https://gitlab.com/ohoral) | 110 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 1 | 500 |
| [@nolith](https://gitlab.com/nolith) | 2 | 300 |
| [@greg](https://gitlab.com/greg) | 3 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 4 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 5 | 200 |
| [@mhuseinbasic](https://gitlab.com/mhuseinbasic) | 6 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 7 | 160 |
| [@smcgivern](https://gitlab.com/smcgivern) | 8 | 80 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 9 | 80 |
| [@rspeicher](https://gitlab.com/rspeicher) | 10 | 60 |
| [@aqualls](https://gitlab.com/aqualls) | 11 | 40 |
| [@rymai](https://gitlab.com/rymai) | 12 | 20 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@lienvdsteen](https://gitlab.com/lienvdsteen) | 1 | 1000 |
| [@tywilliams](https://gitlab.com/tywilliams) | 2 | 300 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@imrishabh18](https://gitlab.com/imrishabh18) | 1 | 1500 |
| [@feistel](https://gitlab.com/feistel) | 2 | 1400 |
| [@JeremyWuuuuu](https://gitlab.com/JeremyWuuuuu) | 3 | 600 |
| [@leetickett](https://gitlab.com/leetickett) | 4 | 500 |
| [@behrmann](https://gitlab.com/behrmann) | 5 | 500 |
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 6 | 300 |
| [@stevemathieu](https://gitlab.com/stevemathieu) | 7 | 300 |
| [@abhijeet007rocks8](https://gitlab.com/abhijeet007rocks8) | 8 | 300 |
| [@cyberap](https://gitlab.com/cyberap) | 9 | 300 |
| [@law-lin](https://gitlab.com/law-lin) | 10 | 300 |
| [@Fall1ngStar](https://gitlab.com/Fall1ngStar) | 11 | 300 |
| [@wwwjon](https://gitlab.com/wwwjon) | 12 | 300 |
| [@tnir](https://gitlab.com/tnir) | 13 | 200 |

## FY22-Q4

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@djadmin](https://gitlab.com/djadmin) | 1 | 1280 |
| [@dblessing](https://gitlab.com/dblessing) | 2 | 1000 |
| [@vitallium](https://gitlab.com/vitallium) | 3 | 900 |
| [@ifrenkel](https://gitlab.com/ifrenkel) | 4 | 700 |
| [@dsearles](https://gitlab.com/dsearles) | 5 | 700 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 6 | 700 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 7 | 500 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 8 | 400 |
| [@ggeorgiev_gitlab](https://gitlab.com/ggeorgiev_gitlab) | 9 | 400 |
| [@matteeyah](https://gitlab.com/matteeyah) | 10 | 300 |
| [@jameslopez](https://gitlab.com/jameslopez) | 11 | 300 |
| [@viktomas](https://gitlab.com/viktomas) | 12 | 300 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 13 | 300 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 14 | 300 |
| [@gonzoyumo](https://gitlab.com/gonzoyumo) | 15 | 300 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 16 | 300 |
| [@mrincon](https://gitlab.com/mrincon) | 17 | 300 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 18 | 260 |
| [@jcaigitlab](https://gitlab.com/jcaigitlab) | 19 | 200 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 20 | 200 |
| [@peterhegman](https://gitlab.com/peterhegman) | 21 | 200 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 22 | 200 |
| [@garyh](https://gitlab.com/garyh) | 23 | 120 |
| [@ekigbo](https://gitlab.com/ekigbo) | 24 | 100 |
| [@manojmj](https://gitlab.com/manojmj) | 25 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 26 | 90 |
| [@pshutsin](https://gitlab.com/pshutsin) | 27 | 80 |
| [@splattael](https://gitlab.com/splattael) | 28 | 60 |
| [@serenafang](https://gitlab.com/serenafang) | 29 | 60 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 30 | 60 |
| [@balasankarc](https://gitlab.com/balasankarc) | 31 | 60 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 32 | 60 |
| [@engwan](https://gitlab.com/engwan) | 33 | 60 |
| [@mkozono](https://gitlab.com/mkozono) | 34 | 60 |
| [@sming-gitlab](https://gitlab.com/sming-gitlab) | 35 | 60 |
| [@mwoolf](https://gitlab.com/mwoolf) | 36 | 50 |
| [@minac](https://gitlab.com/minac) | 37 | 50 |
| [@10io](https://gitlab.com/10io) | 38 | 30 |
| [@ahegyi](https://gitlab.com/ahegyi) | 39 | 30 |
| [@ebaque](https://gitlab.com/ebaque) | 40 | 30 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 41 | 30 |
| [@mksionek](https://gitlab.com/mksionek) | 42 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@greg](https://gitlab.com/greg) | 1 | 300 |
| [@mhuseinbasic](https://gitlab.com/mhuseinbasic) | 2 | 200 |
| [@rspeicher](https://gitlab.com/rspeicher) | 3 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@tywilliams](https://gitlab.com/tywilliams) | 1 | 300 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@imrishabh18](https://gitlab.com/imrishabh18) | 1 | 1500 |
| [@abhijeet007rocks8](https://gitlab.com/abhijeet007rocks8) | 2 | 300 |
| [@cyberap](https://gitlab.com/cyberap) | 3 | 300 |
| [@law-lin](https://gitlab.com/law-lin) | 4 | 300 |
| [@Fall1ngStar](https://gitlab.com/Fall1ngStar) | 5 | 300 |
| [@wwwjon](https://gitlab.com/wwwjon) | 6 | 300 |

## FY22-Q3

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@djadmin](https://gitlab.com/djadmin) | 1 | 1660 |
| [@ali-gitlab](https://gitlab.com/ali-gitlab) | 2 | 1100 |
| [@tkuah](https://gitlab.com/tkuah) | 3 | 1000 |
| [@sabrams](https://gitlab.com/sabrams) | 4 | 730 |
| [@xanf](https://gitlab.com/xanf) | 5 | 620 |
| [@.luke](https://gitlab.com/.luke) | 6 | 620 |
| [@vitallium](https://gitlab.com/vitallium) | 7 | 600 |
| [@philipcunningham](https://gitlab.com/philipcunningham) | 8 | 600 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 9 | 580 |
| [@shreyasagarwal](https://gitlab.com/shreyasagarwal) | 10 | 500 |
| [@ash2k](https://gitlab.com/ash2k) | 11 | 500 |
| [@dsatcher](https://gitlab.com/dsatcher) | 12 | 500 |
| [@wortschi](https://gitlab.com/wortschi) | 13 | 400 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 14 | 400 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 15 | 400 |
| [@brodock](https://gitlab.com/brodock) | 16 | 400 |
| [@balasankarc](https://gitlab.com/balasankarc) | 17 | 300 |
| [@dblessing](https://gitlab.com/dblessing) | 18 | 240 |
| [@toupeira](https://gitlab.com/toupeira) | 19 | 170 |
| [@pshutsin](https://gitlab.com/pshutsin) | 20 | 140 |
| [@mksionek](https://gitlab.com/mksionek) | 21 | 120 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 22 | 100 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 23 | 90 |
| [@mwoolf](https://gitlab.com/mwoolf) | 24 | 90 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 25 | 90 |
| [@acroitor](https://gitlab.com/acroitor) | 26 | 80 |
| [@rcobb](https://gitlab.com/rcobb) | 27 | 80 |
| [@stanhu](https://gitlab.com/stanhu) | 28 | 80 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 29 | 80 |
| [@cngo](https://gitlab.com/cngo) | 30 | 80 |
| [@brytannia](https://gitlab.com/brytannia) | 31 | 80 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 32 | 60 |
| [@jivanvl](https://gitlab.com/jivanvl) | 33 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 34 | 60 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 35 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 36 | 60 |
| [@10io](https://gitlab.com/10io) | 37 | 60 |
| [@minac](https://gitlab.com/minac) | 38 | 60 |
| [@nfriend](https://gitlab.com/nfriend) | 39 | 60 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 40 | 60 |
| [@tomquirk](https://gitlab.com/tomquirk) | 41 | 60 |
| [@serenafang](https://gitlab.com/serenafang) | 42 | 60 |
| [@farias-gl](https://gitlab.com/farias-gl) | 43 | 60 |
| [@afontaine](https://gitlab.com/afontaine) | 44 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 45 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 46 | 40 |
| [@dzaporozhets](https://gitlab.com/dzaporozhets) | 47 | 30 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 48 | 30 |
| [@nmilojevic1](https://gitlab.com/nmilojevic1) | 49 | 30 |
| [@lulalala](https://gitlab.com/lulalala) | 50 | 30 |
| [@ebaque](https://gitlab.com/ebaque) | 51 | 30 |
| [@ohoral](https://gitlab.com/ohoral) | 52 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 1 | 500 |
| [@smcgivern](https://gitlab.com/smcgivern) | 2 | 40 |
| [@rymai](https://gitlab.com/rymai) | 3 | 20 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@lienvdsteen](https://gitlab.com/lienvdsteen) | 1 | 400 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@feistel](https://gitlab.com/feistel) | 1 | 1400 |
| [@behrmann](https://gitlab.com/behrmann) | 2 | 500 |
| [@stevemathieu](https://gitlab.com/stevemathieu) | 3 | 300 |

## FY22-Q2

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@manojmj](https://gitlab.com/manojmj) | 1 | 900 |
| [@pks-t](https://gitlab.com/pks-t) | 2 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 3 | 400 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 4 | 400 |
| [@10io](https://gitlab.com/10io) | 5 | 380 |
| [@leipert](https://gitlab.com/leipert) | 6 | 380 |
| [@markrian](https://gitlab.com/markrian) | 7 | 360 |
| [@ali-gitlab](https://gitlab.com/ali-gitlab) | 8 | 340 |
| [@mksionek](https://gitlab.com/mksionek) | 9 | 320 |
| [@mrincon](https://gitlab.com/mrincon) | 10 | 300 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 11 | 300 |
| [@mikeeddington](https://gitlab.com/mikeeddington) | 12 | 300 |
| [@theoretick](https://gitlab.com/theoretick) | 13 | 300 |
| [@stanhu](https://gitlab.com/stanhu) | 14 | 300 |
| [@tkuah](https://gitlab.com/tkuah) | 15 | 240 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 16 | 200 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 17 | 120 |
| [@engwan](https://gitlab.com/engwan) | 18 | 100 |
| [@mkozono](https://gitlab.com/mkozono) | 19 | 80 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 20 | 80 |
| [@dblessing](https://gitlab.com/dblessing) | 21 | 80 |
| [@tancnle](https://gitlab.com/tancnle) | 22 | 80 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 23 | 80 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 24 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 25 | 70 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 26 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 27 | 60 |
| [@jerasmus](https://gitlab.com/jerasmus) | 28 | 60 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 29 | 60 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 30 | 60 |
| [@jannik_lehmann](https://gitlab.com/jannik_lehmann) | 31 | 60 |
| [@nfriend](https://gitlab.com/nfriend) | 32 | 60 |
| [@dgruzd](https://gitlab.com/dgruzd) | 33 | 40 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 34 | 40 |
| [@f_caplette](https://gitlab.com/f_caplette) | 35 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 36 | 40 |
| [@kerrizor](https://gitlab.com/kerrizor) | 37 | 30 |
| [@ekigbo](https://gitlab.com/ekigbo) | 38 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 39 | 30 |
| [@lulalala](https://gitlab.com/lulalala) | 40 | 30 |
| [@dzaporozhets](https://gitlab.com/dzaporozhets) | 41 | 30 |
| [@ifarkas](https://gitlab.com/ifarkas) | 42 | 30 |
| [@nmezzopera](https://gitlab.com/nmezzopera) | 43 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rspeicher](https://gitlab.com/rspeicher) | 1 | 30 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 2 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@lienvdsteen](https://gitlab.com/lienvdsteen) | 1 | 600 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@JeremyWuuuuu](https://gitlab.com/JeremyWuuuuu) | 1 | 600 |
| [@leetickett](https://gitlab.com/leetickett) | 2 | 500 |
| [@tnir](https://gitlab.com/tnir) | 3 | 200 |

## FY22-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 1 | 1000 |
| [@engwan](https://gitlab.com/engwan) | 2 | 680 |
| [@alexpooley](https://gitlab.com/alexpooley) | 3 | 500 |
| [@theoretick](https://gitlab.com/theoretick) | 4 | 400 |
| [@whaber](https://gitlab.com/whaber) | 5 | 400 |
| [@sabrams](https://gitlab.com/sabrams) | 6 | 300 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 7 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 8 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 9 | 250 |
| [@leipert](https://gitlab.com/leipert) | 10 | 200 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 11 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 12 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 13 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 14 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 15 | 140 |
| [@mksionek](https://gitlab.com/mksionek) | 16 | 140 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 17 | 140 |
| [@twk3](https://gitlab.com/twk3) | 18 | 130 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 19 | 110 |
| [@balasankarc](https://gitlab.com/balasankarc) | 20 | 110 |
| [@stanhu](https://gitlab.com/stanhu) | 21 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 22 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 23 | 100 |
| [@cablett](https://gitlab.com/cablett) | 24 | 100 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 25 | 80 |
| [@vsizov](https://gitlab.com/vsizov) | 26 | 80 |
| [@splattael](https://gitlab.com/splattael) | 27 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 28 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 29 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 30 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 31 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 32 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 33 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 34 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 35 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 36 | 60 |
| [@manojmj](https://gitlab.com/manojmj) | 37 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 38 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 39 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 40 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 41 | 50 |
| [@tomquirk](https://gitlab.com/tomquirk) | 42 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 43 | 40 |
| [@mwoolf](https://gitlab.com/mwoolf) | 44 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 45 | 40 |
| [@mrincon](https://gitlab.com/mrincon) | 46 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 47 | 30 |
| [@proglottis](https://gitlab.com/proglottis) | 48 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 49 | 30 |
| [@cngo](https://gitlab.com/cngo) | 50 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 5 | 50 |
| [@smcgivern](https://gitlab.com/smcgivern) | 6 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 7 | 40 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |


